<?php

namespace SheepItRenderFarm\BlendReader;

include_once 'BlendReader.php';

class BlenderReaderBpy extends BlendReader {

    private $socket_loc;
    private $blend_name;

    public function __construct(string $socket_loc, string $blend_name){
        $this->socket_loc = $socket_loc;
        $this->blend_name = $blend_name;
    }

    public function getInfos(){
        echo $this->blend_name.PHP_EOL;
        $data_arr = array("filename" => $this->blend_name);
        $json_data_arr = json_encode($data_arr);

        $socket = socket_create(AF_UNIX, SOCK_STREAM, SOL_SOCKET) or die("Could not create socket");

        socket_connect($socket, $this->socket_loc) or die("Could not connect to socket");
        socket_send($socket, $json_data_arr, strlen($json_data_arr), 0);
        
        socket_recv($socket, $answer_buff, 1000, 0);
        echo $answer_buff.PHP_EOL;
        
        socket_close($socket);

        return json_decode($answer_buff, true);
    }
}

$default_socket = "uds_socket.sock";

$name = "testfile.blend";

if (isset($argv[1])){
    $name = $argv[1];
}

$blender_reader = new BlenderReaderBpy($default_socket, $name);

$infos = $blender_reader->getInfos();

echo $infos["engine"].PHP_EOL;
